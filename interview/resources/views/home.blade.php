@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div>
                        @if(auth()->user()->role == '1')
                        <a href="{{ URL::to('user/add') }}">Add</a>
                        @endif
                    </div>

                    <table class="table" id="table">
                        <thead>
                            <tr>
                                <th class="text-center">First Name</th>
                                <th class="text-center">User Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Phone NUmber</th>
                                <th class="text-center">Date Of Bath</th>
                                <th class="text-center">Image</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($data) && !empty($data))
                                @foreach ($data as $key => $value)
                                    <tr>
                                        <td>{{$value->first_name}}</td>
                                        <td>{{$value->user_name}}</td>
                                        <td>{{$value->email}}</td>
                                        <td>{{$value->phone}}</td>
                                        <td>{{$value->dob}}</td>
                                        <td>{{$value->first_name}}</td>
                                        <td>
                                            <a href="{{ URL::to('user/edit',$value->id) }}" class="btn btn-primary btn-icon edit" ><i class="far fa-edit"></i> Edit</a>
                                            <?php $url = URL::to('user/delete',$value->id); ?>
                                            @if(auth()->user()->role == '1')
                                                @if($value->role != '1')
                                                    <a href="javascript:void(0);" onclick="action('<?php echo $url; ?>');" class="btn btn-danger btn-icon delete" ><i class="far fa-trash-alt"></i>Delete</a>
                                                @endif

                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                        {{ $data->links() }}
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.0.2/dist/sweetalert2.all.min.js"></script>
<script>
    function action(url){
        var url = url;
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit.fire({
            title: 'Are you sure?',
            text: "You won't Delete this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if(result.value) {
                window.location.href = url;
            }
        });
    }
</script>
@endsection
