@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ route('edituserupdate') }}" id="form" method="post" enctype="multipart/form-data">
                        @csrf
                         <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="@if(isset($data->first_name) && !empty($data->first_name)){{$data->first_name}} @endif" required autocomplete="first_name" autofocus>

                                @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">User Name</label>

                            <div class="col-md-6">
                                <input id="user_name" type="text" class="form-control @error('user_name') is-invalid @enderror" name="user_name" value="@if(isset($data->user_name) && !empty($data->user_name)){{$data->user_name}} @endif" required autocomplete="user_name" autofocus readonly>

                                @error('user_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Phone Number</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="@if(isset($data->phone) && !empty($data->phone)){{$data->phone}} @endif" required autocomplete="phone" autofocus>

                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                         <div class="form-group row" id="items_email">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Email</label>

                            <div class="col-md-6">
                                @if(isset($data->email) && !empty($data->email))
                                    <?php
                                        $email = $data->email;
                                        $email = str_replace(' ', '', trim($email));
                                        $email = explode(",",$email);
                                        $email = array_filter($email);
                                        $destinationPath = asset('/uploads');
                                    ?>
                                    @foreach ($email as $keyE => $emailval1)

                                        @if(!empty($emailval1))
                                            @if($emailval1 != " ")
                                            <div class="form-group ">
                                                <a href="#" class="remove_field">remove</a>
                                                <input type="email" name="email[{{$keyE}}]" value="{{$emailval1}}"  class="form-control @error('email') is-invalid @enderror">
                                                
                                            </div>
                                            @endif
                                        @endif
                                    @endforeach
                                @else
                                    <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email[]" value="@if(isset($data->email) && !empty($data->email)){{$data->email}} @endif" autocomplete="email" autofocus>
                                @endif

                                <button type="button" class="add_field_button">Add Field</button>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Date Of Bith</label>

                            <div class="col-md-6 data_1">
                                    <div class="input-group date">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <?php
                                            $date = null;
                                            if (isset($data->dob) && !empty($data->dob)) {
                                                $date = strtotime($data->dob);
                                                $date = date("d/m/Y",$date);
                                            }
                                        ?>
                                        <input type="text" class="form-control" name="dob" data-mask="00/00/0000" data-mask-selectonfocus="true" id="dob" value="{{$date}}">
                                    </div>

                                @error('dob ')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row" id="items">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Email</label>

                            <div class="col-md-6">
                                <input id="file" type="file" class="form-control @error('email') is-invalid @enderror" name="files[]" value="" accept="image/*" multiple>

                                <?php
                                    $image = $data->image;
                                    $image1 = str_replace(' ', '', trim($image));
                                    $image1 = explode(",",$image);
                                    $image1 = array_filter($image1);
                                    $destinationPath = asset('/uploads');
                                ?>
                                @foreach ($image1 as $key => $val1)

                                    @if(!empty($val1))
                                        @if($val1 != " ")
                                        <div class="form-group ">
                                            <?php
                                            $pic = '';
                                                if (!empty($val1)) {
                                                    $pic = asset('uploads').'/'.$val1;
                                                }
                                                $val1 = trim($val1);
                                            ?>
                                            @if (isset($pic) && !empty($pic))
                                                <img class="form-control col-md-11" id="author_email" src="{{$destinationPath}}/{{$val1}}" />
                                                <a href="#" class="remove_field">remove</a>
                                                <input type="hidden" name="old_files[{{$key}}]" value="{{$val1}}">
                                            @endif
                                        </div>
                                        @endif
                                    @endif
                                @endforeach

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{$id}}">
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    
    $(document).ready(function($) {
        $('.data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: 'dd/mm/yyyy',
            viewMode: 0,
        });
    });

    $(document).ready(function() {
        var wrapper = $("#items"); //Fields wrapper

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove field
            e.preventDefault(); $(this).parent('div').remove();
        })
    });

    $(document).ready(function() {
    var max_fields = 20; //maximum input boxes allowed
    var wrapper = $("#items_email"); //Fields wrapper
    var add_button = $(".add_field_button"); //Add button ID
     
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
    e.preventDefault();
    if(x < max_fields){ //max input box allowed
    x++; //text box increment
    $(wrapper).append('<div class="col-md-12"><div class="form-group"><label for="name" class="col-md-4 col-form-label text-md-right">Email</label><div class="col-md-6">' +
    '<input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email[]" value="" autocomplete="email" autofocus>' +
    '</div><a href="#" class="remove_field">remove</div></div>'); //add input box
    }
    });
     
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove field
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});


</script>
@endsection
