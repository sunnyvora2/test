<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user/add', 'HomeController@userAdd')->name('userAdd');
Route::get('/user/edit/{id}', 'HomeController@userEdit')->name('edituser');
Route::post('/user/update', 'HomeController@userUpdate')->name('edituserupdate');
Route::post('/user/save', 'HomeController@userUpdateSave')->name('saveuserupdate');

Route::get('/user/delete/{id}', 'HomeController@reviewDelete');
