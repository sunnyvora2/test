<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use View;
use Validator;
use Hash;
use Carbon\Carbon;
use Mail;
use Auth;
use File;
use Input;
use URL;
use Config;
use Artisan;
use Symfony\Component\HttpFoundation\Request;
use Socialite;
use App\User;
use Redirect;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $input = $request->all();

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        if(auth()->attempt(array('user_name' => $input['email'], 'password' => $input['password'],'status' => 1)))
        {
            if (auth()->user()->role == '1') {

                Session::flash('admin_operationSuccess', 'Login successfully !');
                return redirect()->route('home');
            }elseif(auth()->user()->role == '2')
            {
                Session::flash('operationSuccess', 'Login successfully !');
                return redirect()->route('home');
            }
        }else{
            Session::flash('admin_operationFailed', 'Something went wrong');
            Session::flash('operationFailed', 'Something went wrong');
            return Redirect::back()->with('error','Email-Address And Password Are Wrong.');
            //return redirect()->route('login')->with('error','Email-Address And Password Are Wrong.');
        }

    }
    protected function logout(Request $request)
    {
        if(Auth::check() && Auth()->user()->role == '1')
        {
            $this->guard()->logout();
            $request->session()->flush();
            $request->session()->regenerate();
            return redirect('/login');
        }
        else{
            $this->guard()->logout();
            $request->session()->flush();
            $request->session()->regenerate();
            return redirect('/login');
        }

    }
}
