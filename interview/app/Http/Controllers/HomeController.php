<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Image;
use File;
use Illuminate\Support\Facades\Hash;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (auth()->user()->role == '1') {
            $data = User::paginate(10);
        }else{
            $id = auth()->user()->id;
            $data = User::where('id', $id)->paginate(10);
        }
        return view('home',compact('data'));
    }
    public function userAdd()
    {
        $id = '';
        return view('add' ,compact('id'));
    }

    public function userEdit($id)
    {
        $data = User::find($id);
        return view('edit',compact('data','id'));
    }
    public function userUpdate(Request $request)
    {
        $post = $request->all();

        $insert_ar = array();
        $insert_ar_img = array();
        $insert_ar_img1 = array();
        $today = date('Y-m-d H:i:s');
        $destinationPath = 'uploads';

        $images = $request->file('files');
        if ($request->hasFile('files')){
                $no = 0;
                foreach ($images as $item){
                    $no++;
                    $var = date_create();
                    $extension = $item->getClientOriginalExtension();
                    $time = date_format($var, 'YmdHis');
                    $imageName = $time.$no.".{$extension}";
                    $upload_success = $item->move($destinationPath, $imageName);

                    $insert_ar_img[] = $imageName;
                }
                if(!empty($insert_ar_img))
                {
                    echo json_encode($insert_ar_img,true);
                }
        }

        $insert_ar_img1[] = '';
        if(!empty($request->old_files)){

            $insert_ar_img1 = $request->old_files;
            $insert_ar_img1 = array_filter($insert_ar_img1);
            
        }

        $image_mearge = array_merge($insert_ar_img,$insert_ar_img1);

        $muti_email = implode(', ', $post['email']);

        $date = null;
        if (isset($post['dob']) && !empty($post['dob'])) {
            $date = $post['dob'];
            $date = str_replace('/', '-', $date);
            $date = date("Y-m-d", strtotime($date));
        }
        $List = implode(', ', $image_mearge);

        $insert_ar['first_name'] = $post['first_name'];
        $insert_ar['user_name'] = $post['user_name'];
        $insert_ar['phone'] = $post['phone'];
        
        $insert_ar['email'] = $muti_email;
        $insert_ar['dob'] = $date;
        $insert_ar['image'] = $List;
        $insert_ar['created_at'] = $today;

        DB::table('users')->where('id',$post['id'])->update($insert_ar);

        return redirect('home')->with('success');
        // return back()->with('success');
    }

    public function reviewDelete($id)
    {

        if(!empty($id))
        {
            $user = User::find($id);

            $user->delete();
        }
        return back()->with('success');
    }

    public function userUpdateSave(Request $request)
    {

        Validator::extend('without_spaces', function($attr, $value){
            return preg_match('/^\S*$/u', $value);
        });
        $validatedData = $request->validate([
            'user_name' => 'required|unique:users|max:25|without_spaces',
            'first_name' => 'required',
            'email' => 'required',
        ]);

        $post = $request->all();

        $insert_ar = array();
        $insert_ar_img = array();
        $insert_ar_img1 = array();
        $today = date('Y-m-d H:i:s');
        $destinationPath = 'uploads';

        $images = $request->file('files');
        if ($request->hasFile('files')){
                $no = 0;
                foreach ($images as $item){
                    $no++;
                    $var = date_create();
                    $extension = $item->getClientOriginalExtension();
                    $time = date_format($var, 'YmdHis');
                    $imageName = $time.$no.".{$extension}";
                    $upload_success = $item->move($destinationPath, $imageName);

                    $insert_ar_img[] = $imageName;
                }
                if(!empty($insert_ar_img))
                {
                    echo json_encode($insert_ar_img,true);
                }
        }


        $muti_email = implode(', ', $post['email']);

        $date = null;
        if (isset($post['dob']) && !empty($post['dob'])) {
            $date = $post['dob'];
            $date = str_replace('/', '-', $date);
            $date = date("Y-m-d", strtotime($date));
        }
        $List = implode(', ', $insert_ar_img);

        $insert_ar['first_name'] = $post['first_name'];
        $insert_ar['user_name'] = $post['user_name'];
        $insert_ar['phone'] = $post['phone'];
        $insert_ar['email'] = $muti_email;
        $insert_ar['password'] = Hash::make('123456789');
        $insert_ar['dob'] = $date;
        $insert_ar['image'] = $List;
        $insert_ar['created_at'] = $today;
        $insert_ar['role'] = 2;
        $insert_ar['status'] = 1;
            DB::table('users')->insert($insert_ar);

        return redirect('home')->with('success');
    }
}
