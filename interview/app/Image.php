<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'image';

	protected $primaryKey = 'i_id';

	protected $fillable = [
        'u_id','image_name', 'created_at'
    ];
}
